# Selenium-Automation-testing-with-TestNG

TestNG is a testing framework inspired from JUnit but introducing some new functionality that makes it more powerful and easier to use.

By the end of this course, you will have a great understanding of the TestNG framework.

How to use the TestNG framework to run test-cases in parallel or in a multithreaded environment and will get detailed knowledge about TestNG framework. This tutorial will give you great understanding on all TestNG Framework concepts. 

Selenium Automation testing with TestNG is designed to cover all concepts of testing with TestNG framework:

Annotations

Unit Testing

Functional Testing