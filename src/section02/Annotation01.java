package section02;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Annotation01 {
	
  @BeforeMethod
  public void intialize() {
	  System.out.println("Inside BeforeMethod");
  }
	
  @Test
  public void Login() {
	  System.out.println("Inside Login");
  }
  @Test
  public void Logout() {
	  System.out.println("Inside Logout");
  }
  
  @AfterMethod
  public void close() {
	  System.out.println("Inside AfterMethod");
  }
}
