package section02;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Annotation02 {
  @BeforeClass
  public void connect() {
	  System.out.println("Inside BeforeClass");
  }
  @BeforeMethod
  public void intialize() {
	  System.out.println("Inside BeforeMethod");
  }
	
  @Test
  public void Login() {
	  System.out.println("Inside Login");
  }
  @Test
  public void Logout() {
	  System.out.println("Inside Logout");
  }
  
  @AfterMethod
  public void close() {
	  System.out.println("Inside AfterMethod");
  }
  
  @AfterClass
  public void Terminate() {
	  System.out.println("Inside AfterClass");
  }
}
