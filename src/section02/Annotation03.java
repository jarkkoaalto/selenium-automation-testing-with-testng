package section02;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Annotation03 {
  @BeforeSuite
  public void suitebefore() {
	  System.out.println("Inside before suite");
  }
  @BeforeTest
  public void before() {
	  System.out.println("Before Test");
  }
  @BeforeClass
  public void connect() {
	  System.out.println("Inside BeforeClass");
  }
  @BeforeMethod
  public void intialize() {
	  System.out.println("Inside BeforeMethod");
  }
  @Test
  public void Login() {
	  System.out.println("Inside Login");
  }
  @Test
  public void Logout() {
	  System.out.println("Inside Logout");
  }
  @AfterMethod
  public void close() {
	  System.out.println("Inside AfterMethod");
  }
  @AfterClass
  public void Terminate() {
	  System.out.println("Inside AfterClass");
  }
  @AfterTest
  public void After() {
	  System.out.println("After test");  
  }
  @AfterSuite
  public void suiteafter() {
	  System.out.println("Inside after suite");
  }
}
