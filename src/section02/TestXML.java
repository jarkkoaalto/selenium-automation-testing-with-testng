package section02;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestXML {
  @BeforeMethod
  public void initalize() {
	  System.out.println("Inside BeforeMethod");
  }
  
  @Test
  public void Login() {
	  System.out.println("Inside login");
  }
  @Test
  public void Logout() {
	  System.out.println("Inside Logout");
  }
  @AfterMethod
  public void Close() {
	  System.out.println("Inside AfterMethod");
  }
}
