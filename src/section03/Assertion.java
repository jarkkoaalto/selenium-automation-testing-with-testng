package section03;

import org.testng.Assert;
import org.testng.annotations.Test;

public class Assertion {
  @Test(priority=1)
  public void openloginpage() {
	  System.out.println("Open login page");
  }
  @Test(priority=2)
  public void checkLogin() {
	  System.out.println("Test login");
//	  Assert.assertEquals("logged in successfully", "successfull");
	  Assert.assertEquals("logged in successfully", "logged in successfully");
  }
  @Test(priority=3,dependsOnMethods="checkLogin")
  public void logout() {
	  System.out.println("Logout ...");
  }
}
