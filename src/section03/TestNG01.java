package section03;

import org.testng.annotations.Test;

public class TestNG01 {
  @Test(groups="login")
  public void enterUsername() {
	  System.out.println("Enter username");
  }
  @Test(groups="login",dependsOnMethods = "enterUsername")
  public void enterPassword() {
	  System.out.println("Enter password");
  }
  @Test(groups="login" ,dependsOnMethods = "enterPassword")
  public void clickonlogin() {
	  System.out.println("Click on login button");
  }
  @Test(groups="search" ,dependsOnMethods = "clickonlogin")
  public void selectlocation() {
	  System.out.println("Select Location");
  }
  @Test(groups="search", dependsOnMethods = "selectlocation")
  public void selectpreferences() {
	  System.out.println("Select Performance");
  }
  @Test(groups="search", dependsOnMethods = "selectpreferences")
  public void selectDate() {
	  System.out.println("Select dates");
  }
  @Test(groups="search",dependsOnMethods = "selectDate")
  public void clickonsearch() {
	  System.out.println("Click on search");
  }
  @Test(groups="logout", dependsOnMethods = "clickonsearch")
  public void clickonlogout() {
	  System.out.println("Click on Logout");
	  System.out.println("Logout from flight reservation system");
  }
}
