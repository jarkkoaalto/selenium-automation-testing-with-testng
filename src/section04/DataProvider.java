package section04;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;



public class DataProvider {
	private WebDriver driver;
	private String url;
	
	@BeforeClass
	public void openBeowser() {
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\Jarkko\\workspace\\TestNG\\geckodriver.exe");
		url="http://newtours.demoaut.com/";
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	
	}

	
	@Test(dataProvider = "Parameters",groups="login")
	public void Login(String username, String password) {
		driver.get(url = "/");
		driver.findElement(By.name("userName")).clear();
		driver.findElement(By.name("userName")).sendKeys(username);
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys(password);
		driver.findElement(By.name("login")).click();
		logout();
  }
	
	public void logout() {
		driver.findElement(By.linkText("SIGN-OFF")).click();
	}
	
	//@dataProvider
	public Object[][] Parameters(){
		return new Object[][] {{"infotek","infotek"},{"qatraining","qatraining"}};
	}
	
	@AfterClass
	public void closeBorwser() {
		driver.quit();
	}
	
}
