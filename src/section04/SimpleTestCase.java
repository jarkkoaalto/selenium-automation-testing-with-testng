package section04;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SimpleTestCase {
  @Test(dataProvider="Parameters")
  public void f(String username, String password) {
	  System.out.println(username + " " + password);
  }
  
  @DataProvider
  public Object[][] Parameters(){
	  return new Object[][] {{"infotek","infotek"},{"qatrining","qatrining"}};
  }
}
