package section05;

import org.testng.IExecutionListener;
import org.testng.annotations.Test;

public class ExecutionListener01 implements IExecutionListener {
  private long startTime;

@Override
public void onExecutionStart() {
	// TODO Auto-generated method stub
	startTime = System.currentTimeMillis();
	System.out.println("TestNG is going to start");
}

@Override
public void onExecutionFinish() {
	// TODO Auto-generated method stubst
	System.out.println("TestNG has finished, took around " + (System.currentTimeMillis() - startTime)+" ms");
	
}
  
 
}
