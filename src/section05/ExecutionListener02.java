package section05;

import org.testng.IExecutionListener;
import org.testng.annotations.Test;

import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

public class ExecutionListener02 implements IExecutionListener{
	private long startTime;

@Override
public void onExecutionStart() {
	// TODO Auto-generated method stub
	startTime = System.currentTimeMillis();
	System.out.println("TestNG is going to start");
	
}

@Override
public void onExecutionFinish() {
	// TODO Auto-generated method stub
	System.out.println("TestNG has finishe, took around " + (System.currentTimeMillis() -startTime)+ " ms");
	
}
}
