package section05;

import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.annotations.Test;

public class Test33withTestListener implements ISuiteListener {
  @Test
  public void f() {
  }

@Override
public void onStart(ISuite suite) {
	// TODO Auto-generated method stub
	System.out.println("inside OnStart");
	System.out.println("TestNG suite default output directory = "+ suite.getOutputDirectory());
}

@Override
public void onFinish(ISuite suite) {
	// TODO Auto-generated method stub
	System.out.println("Inside finish");
	System.out.println("TestNG invoked methods = " + suite.getAllInvokedMethods());
}
}
