package section05;

import org.testng.annotations.Test;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class TestClass01 {
  @BeforeSuite
  public void beforeSuite() {
	  System.out.println("BeforeSuite");
  }
  @Test
  public void t() {
	  System.out.println("Test");
  }
  @AfterSuite
  public void afterSuite() {
	  System.out.println("AfterSuite");
  }
  
}
