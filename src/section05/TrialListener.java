package section05;

import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.annotations.Test;

public class TrialListener implements ISuiteListener{
  @Test
  public static void main(String[]args) {
  }

@Override
public void onStart(ISuite suite) {
	// TODO Auto-generated method stub
	System.out.println("TestNG suite default output directory = " + suite.getOutputDirectory());
}

@Override
public void onFinish(ISuite suite) {
	// TODO Auto-generated method stub
	System.out.println("TestNG invoke methods = " + suite.getAllInvokedMethods());
}
}
